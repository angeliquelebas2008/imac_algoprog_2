#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;



struct BinarySearchTree : public BinaryTree
{    
    BinarySearchTree* left;
    BinarySearchTree* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children
        this->left=NULL;
        this->right=NULL;
        this->value=value;
    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child$

        BinarySearchTree* nouveau=new BinarySearchTree(value);
        nouveau->initNode(value);

        BinarySearchTree* temp = this;
        while(temp){
            // c'est une feuille
            if (temp->isLeaf()==true) {
                if (temp->value > value) {
                    // Insère à gauche
                    temp->left = nouveau;
                    break;
                }
                else {
                    // Insère à droite
                    temp->right = nouveau;
                    break;
                }
            }
            else {
                // ce n'est pas une feuille
                if (temp->value > value) {
                    //on regarde à gauche
                    if (temp->left == NULL) {
                        temp->left = nouveau;
                        break;
                    }
                    temp = temp->left;
                }
                else {
                    //on regarde à droite
                    if (temp->right == NULL) {
                        temp->right = nouveau;
                        break;
                    }
                    temp = temp->right;
                }
            }
        }
    }


	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        if (this->isLeaf()==true)
        {
            return 1;
        }
        else
        {
            uint hauteur_d=0;
            uint hauteur_g=0;

            if(this->left)
            {
                hauteur_g=(this->left)->height();
            }
            if(this->right)
            {
                hauteur_d=(this->right)->height();
            }

            if(hauteur_g>hauteur_d)
            {
                return(hauteur_g+1);
            }
            else
            {
                return(hauteur_d+1);
            }
        }
    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        if(this->isLeaf()==true)
        {
            return 1;
        }
        else
        {
            int taille=1;
            if(this->left)
            {
                taille+=this->left->nodesCount();
            }
            if(this->right)
            {
                taille+=this->right->nodesCount();
            }
            return taille;
        }
	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
         if (!this->right && !this->left) {
             return true;
         }
        return false;
	}

    void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
        if(this->isLeaf())
        {
            leaves[leavesCount]=this;
            leavesCount++;
        }
        else{
            if(this->left)
            {
                this->left->allLeaves(leaves, leavesCount);
            }
            if(this->right)
            {
                this->right->allLeaves(leaves, leavesCount);
            }
        }

	}

    void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
        if(this->isLeaf())
        {
            nodes[nodesCount]=this;
            nodesCount++;
        }
        else
        {
            if(this->left)
            {
                (this->left)->inorderTravel(nodes, nodesCount);
            }
            nodes[nodesCount]=this;
            nodesCount++;
            if(this->right)
            {
                (this->right)->inorderTravel(nodes, nodesCount);
            }
        }

    }

    void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
        if(this->isLeaf())
        {
            nodes[nodesCount]=this;
            nodesCount++;
        }
        else
        {
            nodes[nodesCount]=this;
            nodesCount++;
            if(this->left)
            {
                this->left->preorderTravel(nodes, nodesCount);
            }
            if(this->right)
            {
                this->right->preorderTravel(nodes, nodesCount);
            }
        }
	}

    void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
        if(this->isLeaf()==true)
        {
            nodes[nodesCount]=this;
            nodesCount++;
        }
        else
        {

            if(this->left)
            {
                this->left->postorderTravel(nodes, nodesCount);
            }
            if(this->right)
            {
                this->right->postorderTravel(nodes, nodesCount);
            }
            nodes[nodesCount]=this;
            nodesCount++;
        }
	}

    BinarySearchTree* find(int value) {
        // find the node containing value
        if(this->isLeaf()==true)
        {
            return nullptr;
        }
        if(this->value==value)
        {
            return this;
        }
        else if (this->left && this->value>value)
        {
            return this->left->find(value);
        }
        else if(this->right)
        {
            return this->right->find(value);
        }
        return nullptr;
	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    BinarySearchTree(int value) : BinaryTree(value) {initNode(value);}
    ~BinarySearchTree() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

BinarySearchTree* createNode(int value) {
    return new BinarySearchTree(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<BinarySearchTree>();
	w->show();

	return a.exec();
}
