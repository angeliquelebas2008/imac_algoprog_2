#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
    if(origin.size()<=1)
    {
        return;
    }
    else
    {

	// initialisation
	Array& first = w->newArray(origin.size()/2);
    Array& second = w->newArray(origin.size()-first.size());

    int originSize=origin.size();
    int firstSize=first.size();
    int secondSize=second.size();

	// split
    int i;
    for(i=0; i<firstSize; i++)
    {
        first[i]=origin[i];
    }
    for(int j=0; j<secondSize; i++ && j++)
    {
        second[j]=origin[i];
    }


	// recursiv splitAndMerge of lowerArray and greaterArray
    //if(firstSize>1)
    //{
        splitAndMerge(first);
    //}
    //if(secondSize>1)
    //{
        splitAndMerge(second);
    //}


	// merge
     //Array& result = w->newArray(origin.size());
     merge(first, second, origin);
    }
}

void merge(Array& first, Array& second, Array& result)
{
    int firstSize=first.size();
    int secondSize=second.size();

    int i_first=0;
    int i_second=0;
    int i;

    for (i=0; i<(firstSize+secondSize); i++)
    {
        int minimum;
        if(i_first<firstSize && i_second<secondSize)
        {
            if(first[i_first]<=second[i_second])
            {
                minimum=first[i_first];
                i_first++;
            }
            else
            {
                if(first[i_first]>second[i_second])
                {
                    minimum=second[i_second];
                    i_second++;
                }
            }
        }
        else
        {
            if(i_first<firstSize)
            {
                minimum=first[i_first];
                i_first++;
            }
            if(i_second<secondSize)
            {
                 minimum=second[i_second];
                i_second++;
            }
        }
       result[i]=minimum;
    }
}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
