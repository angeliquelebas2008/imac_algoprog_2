#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    // your code
};

struct DynaTableau{
    int* donnees;
    // your code
};


void initialise(Liste* liste)
{
    if(!liste)
    {
        exit(EXIT_FAILURE);
    }
    liste->premier=NULL;
}

bool est_vide(const Liste* liste)
{
    if(liste->premier)
    {
        return false;
    }
    else
    {
        return true;
    }
}

void ajoute(Liste* liste, int valeur)
{
    Noeud *noeud=(Noeud*)malloc(sizeof(*noeud));
    if(!noeud)
    {
        exit(EXIT_FAILURE);
    }
    noeud->donnee=valeur;
    noeud->suivant=NULL;

    if(est_vide(liste)==false)
    {
        Noeud *pointeur = liste->premier;
        while(pointeur->suivant)
        {
            pointeur=pointeur->suivant;
        }
        pointeur->suivant=noeud;
    }
    else
    {
        liste->premier=noeud;
    }

}

void affiche(const Liste* liste)
{
    Noeud *pointeur=liste->premier;
    while(pointeur)
    {
        std::cout << pointeur->donnee << std::endl;
        pointeur=pointeur->suivant;
    }
}

int recupere(const Liste* liste, int n)
{
    Noeud *pointeur=liste->premier;
    int i=0;
    while(pointeur && i<n)
    {
        pointeur=pointeur->suivant;
        i++;
    }
    if (pointeur)
    {
        return pointeur->donnee;
    }
    return 0;
}

int cherche(const Liste* liste, int valeur)
{
    Noeud *pointeur=liste->premier;
    int n=1;
    while(pointeur && pointeur->donnee!=valeur)
    {
        pointeur=pointeur->suivant;
        n++;
    }
    if (pointeur->donnee==valeur)
    {
        return n;
    }
    return -1;
}

void stocke(Liste* liste, int n, int valeur)
{
    Noeud *pointeur=liste->premier;
    int i=1;
    while(pointeur && i<n)
    {
        pointeur=pointeur->suivant;
        i++;
    }

    pointeur->donnee=valeur;
}

void ajoute(DynaTableau* tableau, int valeur)
{
    int i=0;
    while(tableau->donnees[i])
    {
        i++;
    }
    tableau->donnees[i]=valeur;

}


void initialise(DynaTableau* tableau, int capacite)
{
    if(!tableau)
    {
        exit(EXIT_FAILURE);
    }

    for(int i=0; i<capacite; i++)
    {
        tableau->donnees[i]=NULL;
    }

}

bool est_vide(const DynaTableau* liste)
{
    int i=0;
    while(liste->donnees[i])
    {
        if(liste->donnees[i+1])
        {
            return false;
        }
        i++;
    }

    return true;

}

void affiche(const DynaTableau* tableau)
{
    int i=0;
    while(tableau->donnees[i])
    {
        std::cout << tableau->donnees[i] << std::endl;
        i++;
    }

}

int recupere(const DynaTableau* tableau, int n)
{
    int i=0;
    while(i<n && tableau->donnees[i])
    {
        i++;
    }
    if(i==n)
    {
        return tableau->donnees[i];
    }
    else
    {
        return -1;
    }
}

int cherche(const DynaTableau* tableau, int valeur)
{
    int i=0;
    while(tableau->donnees[i] && tableau->donnees[i]!=valeur)
    {
        i++;
    }
    if(tableau->donnees[i]==valeur)
    {
        return i+1;
    }
    else
    {
        return -1;
    }
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    int i=0;
    while(i<n && tableau->donnees[i])
    {
        i++;
    }
    if(i==n)
    {
        tableau->donnees[i-1]=valeur;
    }
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    Noeud *noeud=(Noeud*)malloc(sizeof(*noeud));
    if(!noeud)
    {
        exit(EXIT_FAILURE);
    }
    noeud->donnee=valeur;
    noeud->suivant=NULL;

    if(est_vide(liste)==false)
    {
        Noeud *pointeur = liste->premier;
        while(pointeur->suivant)
        {
            pointeur=pointeur->suivant;
        }
        pointeur->suivant=noeud;
    }
    else
    {
        liste->premier=noeud;
    }
}

//int retire_file(Liste* liste)
/* ENLEVE LA DERNIERE VALEUR AJOUTEE ET LA RETOURNE !!!
int retire_file(Liste* liste)
{

     if(!liste)
    {
        exit(EXIT_FAILURE);
    }

    if(est_vide(liste)==false)
    {
        int valeur=0;
        Noeud *pointeur = liste->premier;
        Noeud *precedent = liste->premier;
        int taille=1;

        while(pointeur->suivant)
        {
            pointeur=pointeur->suivant;
            taille+=1;
        }
        if (liste && liste->premier && taille>1)
        {
            valeur= pointeur->donnee;

            for (int i=taille; i>2; i--)
            {
                precedent=precedent->suivant;
            }
            precedent->suivant=NULL;
            free(pointeur);
        }
        else
        {
            valeur= precedent->donnee;
            liste->premier=NULL;
        }

       return valeur;
    }
}
*/

int retire_file(Liste* liste)
  {
    if(!liste)
    {
        exit(EXIT_FAILURE);
    }

    int valeur=0;
    Noeud *pointeur= liste->premier;

    if(liste && liste->premier)
    {
        valeur= pointeur->donnee;

        liste->premier=pointeur->suivant;
        free(pointeur);
    }

    return valeur;

}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    Noeud *noeud=(Noeud*)malloc(sizeof(*noeud));
    if(!noeud)
    {
        exit(EXIT_FAILURE);
    }
    noeud->donnee=valeur;

    if(liste->premier)
    {
        noeud->suivant=liste->premier;
        liste->premier=noeud;
    }
    else
    {
        noeud->suivant=NULL;
        liste->premier=noeud;
    }

}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{
    if(!liste)
    {
        exit(EXIT_FAILURE);
    }

    int valeur=0;
    Noeud *pointeur= liste->premier;

    if(liste && liste->premier)
    {
        valeur= pointeur->donnee;

        liste->premier=pointeur->suivant;
        free(pointeur);
    }

    return valeur;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;
    initialise(&pile);
    initialise(&file);


    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }
    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }
    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }
    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }
    return 0;
}
