#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;

void selectionSort(Array& toSort){
	// selectionSort
    for (uint i=0; i<toSort.size(); i++)
    {
        int min=toSort.get(i);
        for (uint j=i; j<toSort.size(); j++)
        {
            //chercher le minimum à partir de i
            if (toSort[j] < min)
                //inverser le minimum et la case courante
                {
                    min=toSort.get(j);
                    toSort.swap(i, j);
                }
        }

    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
