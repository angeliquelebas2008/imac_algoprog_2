#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;

void insertionSort(Array& toSort){
    Array& sorted=w->newArray(toSort.size());//tableau vide
    sorted[0]=toSort[0];
	// insertion sort from toSort to sorted
    for (uint n=1; n<toSort.size(); n++) //n=nombre d'élément dans sorted
        {
            uint m;
            for (m=0; m<n; m++)
            {
                if (sorted[m] > toSort[n])
                {
                    break;
                }
            }
            sorted.insert(m,toSort[n]); //insérer n à la position de m (en décalant donc le reste du tableau)
        }

	toSort=sorted; // update the original array
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(insertionSort); // window which display the behavior of the sort algorithm
	w->show();

	return a.exec();
}
